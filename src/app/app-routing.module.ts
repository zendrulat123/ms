import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './header/home/home.component';
import {GoComponent} from './header/Go/Go.component';
const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'go', component: GoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
